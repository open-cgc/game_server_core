class GameServerCore(object):
    def __init__(self, Mechanic, strategies, on_world_update=None,
                 on_game_over=None):
        self.strategies = strategies
        self.mechanic = Mechanic()
        self.mechanic.add_strategies(strategies)
        self.result = None
        self.log_data = None
        self.on_world_update = on_world_update
        if on_world_update is not None and not callable(on_world_update):
            raise TypeError('on_world_update is not a function')
        self.on_game_over = on_game_over
        if on_game_over is not None and not callable(on_game_over):
            raise TypeError('on_game_over is not a function')

    def setup_strategies(self):
        for strategy in self.strategies:
            strategy.setup()

    def start(self):
        self.setup_strategies()

        while True:
            moving_data_list = self.get_moving_data()

            for moving_data in moving_data_list:
                self.mechanic.update(moving_data['data'], moving_data['player_id'])
            self.mechanic.complete_world_updating()
            if self.on_world_update:
                self.on_world_update(self.mechanic.get_world())
            if self.mechanic.is_game_over():
                self.result = self.mechanic.get_result()
                self.log_data = self.mechanic.get_log_data()
                break
        print('Game over!')
        for strategy in self.strategies:
            strategy.goodbye()
        if self.on_game_over:
            self.on_game_over(self.log_data, self.result)

    def get_moving_data(self):
        moving_data_list = []
        for strategy in self.strategies:
            game_state_data = self.mechanic.get_game_state_data(strategy.player_id)
            moving_data = strategy.update(game_state_data)
            moving_data_list.append({
                'player_id': strategy.player_id,
                'data': moving_data
            })
        return moving_data_list
