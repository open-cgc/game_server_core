import socket
import struct


class NetworkStrategyAdapter(object):
    def __init__(self, player_id, port):
        self.sock = socket.socket()

        print('bind...')
        self.sock.bind(('', port))

        self.player_id = player_id

    def setup(self):
        print('listen...')
        self.sock.listen()
        self.conn, self.addr = self.sock.accept()

    def send_game_state_data(self, game_state_data):
        data_len = len(game_state_data)

        self.conn.send(struct.pack('H', data_len))
        self.conn.send(game_state_data)

    def get_moving_data(self):
        data_len = struct.unpack('H', self.conn.recv(2))[0]
        data = self.conn.recv(data_len)
        return data

    def update(self, game_state_data):
        self.send_game_state_data(game_state_data)
        return self.get_moving_data()

    def goodbye(self):
        self.conn.close()
